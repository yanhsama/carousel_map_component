/**
 * 
 * @param {*} areaDom 轮播区域
 * @param {*} option 轮播配置
 */
function createBannerArea(areaDom,options){
    var imgArea=document.createElement('div');//图片区域div
    var numArea=document.createElement('div');//角标区域div
    var leftDom=document.createElement('div');
    var rightDom=document.createElement('div');

    var curIndex=0;//当前显示的图片
    //创建区域，用于显示图片
    initImgs();
    //创建一个区域。用于显示角标
    initNum()
    //状态
    setStatus()

    initSides()


    /**
     * 局部函数
     */
    //图片区域
    function initImgs(){
        imgArea.style.width="100%";
        imgArea.style.height="100%";
        imgArea.style.display="flex";
        for(let i=0;i<options.length;i++){
            var obj=options[i];
            var img=document.createElement('img');
            img.style.height="100%";
            img.style.width="100%";
            img.style.marginLeft='0';
            img.src=obj.imgUrl;
            imgArea.appendChild(img);
            console.log(obj)
        }
       areaDom.appendChild(imgArea)
    }
    //角标区域
    function initNum(){
        numArea.style.textAlign="center"
        numArea.style.marginTop="-20px"
        for(let i=0;i<options.length;i++){
            var sp=document.createElement('span');
            sp.style.display="inline-block";
            sp.style.height="12px";
            sp.style.width="12px";
            sp.style.backgroundColor='black';
            sp.style.borderRadius="50%";
            sp.style.margin="0 5px";
            sp.style.cursor="pointer";
            sp.addEventListener("click",function(){
                curIndex=i;
                setStatus();
            })
            numArea.appendChild(sp);
        }
        areaDom.appendChild(numArea);
    }
    //角标状态
    function setStatus(){
        //设置角标背景颜色
        for(let i=0;i<numArea.children.length;i++){
            if(i===curIndex){
                //当前显示的轮播图
                numArea.children[i].style.backgroundColor='red';

            }else{
                numArea.children[i].style.backgroundColor='black';
                //当前不显示的
            }
        }

        //显示图片
        var targetMarginLeft=curIndex * -100;
        imgArea.children[0].style.marginLeft=targetMarginLeft+"%";
    }
    
    //左右点击区域
    function initSides(){
        //左边区域
        leftDom.style.backgroundImage="url(./icon/zuojiantou_1.png)"
        leftDom.style.position="absolute"
        leftDom.style.zIndex="99"
        leftDom.style.width="32px";
        leftDom.style.height="32px";
        //leftDom.style.backgroundColor="black";
        leftDom.style.marginTop="-160px"
        leftDom.style.marginLeft="10px"
        leftDom.style.cursor="pointer"
        areaDom.appendChild(leftDom)
        leftDom.addEventListener("click",function(){
            if(curIndex<1){
                curIndex=options.length-1;
                setStatus()
            }else{
                curIndex--
                setStatus()
            }   
        })
        //右边区域
        rightDom.style.backgroundImage="url(./icon/changyongicon-.png)"
        rightDom.style.zIndex="99"
        rightDom.style.position="absolute"
        rightDom.style.width="32px";
        rightDom.style.height="32px";
       // rightDom.style.backgroundColor="black";
        rightDom.style.marginTop="-160px"
        rightDom.style.marginLeft="352px"
        rightDom.style.cursor="pointer"
        areaDom.appendChild(rightDom);
        rightDom.addEventListener("click",function(){
            if(curIndex>options.length-2){
                curIndex=0;
                setStatus()
            }else{
                curIndex++
                setStatus()
            }     
        })
    }
}

            /**
             * 闭包实现
             */
            // (function(index){
            //     sp.addEventListener("click",function(){
            //         curIndex=index;
            //         setStatus();
            //     })
            // })(i)